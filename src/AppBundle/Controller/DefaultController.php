<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository("AppBundle:Product")->findAll();

        return $this->render('@App/index.html.twig', array(
            'products' => $products
        ));
    }

    /**
     * @Route("/item/{name}", name="product")
     */
    public function productAction(Request $request, $name)
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository("AppBundle:Product")->findProductWithCategories($name);
        return $this->render('@App/product.html.twig', array(
            'name' => $name,
            'products' => $products
        ));
    }

    /**
     * @Route("/category/{name}", name="category")
     */
    public function categoryAction(Request $request, $name)
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository("AppBundle:Product")->findProductsByCategoryName($name);

        return $this->render('@App/category.html.twig', array(
            'category' => $name,
            'products' => $products
        ));
    }


    /**
     * @Route("/maket", name="maket")
     */
    public function maketAction(Request $request)
    {
        return $this->render('@App/maket.html.twig', array(
        ));
    }
}
